 SELECT maker FROM product
WHERE type NOT IN('laptop') AND type IN('pc');

select distinct maker from product
WHERE maker NOT IN(SELECT maker FROM product WHERE type = 'laptop') AND
type IN('pc');

select distinct maker from product
WHERE  maker = ALL(SELECT distinct maker FROM product WHERE maker NOT IN(SELECT maker from product WHERE type = 'laptop') AND type = 'pc');

SELECT distinct maker FROM product
WHERE maker = ANY(SELECT maker FROM product WHERE type IN('pc','laptop'));

SELECT class,
CASE
WHEN country = 'Ukraine' THEN "Ukraine"
ELSE country
END
FROM classes;

SELECT COUNT(pc.model) FROM pc
JOIN product p
ON p.model = pc.model
WHERE maker IN('A');

SELECT distinct maker FROM product
WHERE EXISTS (SELECT distinct p.maker FROM product p, pc WHERE pc.model= p.model AND pc.speed>750);

SELECT AVG(price) 'середня ціна' FROM laptop;

SELECT CONCAT('код:',code) code, CONCAT('модель:',model) model, CONCAT('швидкість:',speed)speed,
CONCAT('оперативна:',ram) ram, CONCAT('ціна:', price) FROM pc;

SELECT DATE_FORMAT(date,'%Y.%m.%d') date FROM income;

SELECT p.type, l.model, l.speed FROM laptop l, product p
WHERE p.model = l.model AND
speed<ANY(SELECT speed FROM pc);

SELECT p.maker, MIN(pr.price) price FROM product p, printer pr
WHERE p.model = pr.model AND
pr.color = 'y';

SELECT DISTINCT p.maker,
(SELECT COUNT(pc.model) FROM pc,product  WHERE pc.model= product.model AND product.maker = p.maker ) AS pc,
(SELECT COUNT(laptop.model) FROM laptop,product WHERE laptop.model=product.model AND product.maker=p.maker)AS laptop,
(SELECT COUNT(printer.model) FROM printer,product WHERE printer.model=product.model AND product.maker=p.maker)AS printer
FROM product p;


SELECT DISTINCT p.maker,
(SELECT AVG(screen) FROM laptop l, product pro WHERE l.model = pro.model AND pro.maker = p.maker) AS avg_screen
FROM product p;

SELECT DISTINCT p.maker,
(SELECT MAX(price) FROM pc, product pro WHERE pc.model = pro.model AND pro.maker = p.maker) AS max_price
FROM product p;

SELECT DISTINCT p.maker,
(SELECT MIN(price) FROM pc, product pro WHERE pc.model = pro.model AND pro.maker = p.maker) AS min_price
FROM product p;

SELECT DISTINCT pc.speed,
AVG(price) avg_price
FROM pc
WHERE speed > 600
GROUP BY speed;

